#!/bin/sh

# Before running this script, build the docker container:
# $ docker build -t delightfulclub:latest ./

# Create a temporary backup
cp -r ./website/ ./website-temp/

# Clean the website folder
git clean -fX ./website/content/
rm -r ./website/public

# Fetch updated lists
docker run --rm -u "$(id -u):$(id -g)" -v $PWD:/app -w /app delightfulclub delightfulclub

# Render the static website
if docker run --rm -u "$(id -u):$(id -g)" -v $PWD:/app -w /app/website ghcr.io/getzola/zola:v0.15.3 build ; then
  # If the render was successful, remove the backup
  rm -r ./website-temp
else
  # If the render failed, restore the backup
  rm -r ./website
  mv ./website-temp ./website
fi