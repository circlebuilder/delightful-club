+++
title = "Delightful Club"
sort_by = "title"
template = "index.html"
page_template = "page.html"
+++

## About delightful lists

The delightful project is an initiative by [Arnold Schrijver](https://community.humanetech.com/u/aschrijver/summary).

From [teaserbot-labs/delightful on codeberg](https://codeberg.org/teaserbot-labs/delightful):

> The internet can be a wonderful place. A place that unites people, to exchange ideas. It allows people to cooperate and unleash their creativity. This brought forth great source of information and knowledge, open science and many beautiful free software projects to enrich our lives with. The internet has the potential to bring our minds together. To set us free.
> 
> But increasingly commercial interests are thwarting the vision of what the internet can bring to humanity. The gems of free knowledge and open source software are harder to find these day. Drowned out by marketing, advertising and SEO they do not appear in the top of our search results anymore.
> 
> Delightful lists are an effort to help bring change to this trend. To make freedom more discoverable again. This top-level project will allow navigation to all high-quality curated delightful lists created and maintained all over the web.
> 
> Anyone that wishes to do so can create their own list, and thus create an entrypoint to freedom.
>
> [...]
>
> The whole concept of delightful lists was inspired by the popular awesome project on Github, that was started by [Sindre Sorhus](https://sindresorhus.com/). Read more in our FAQ.

## About delightful.club

[delightful.club](/) is maintained by [Yarmo Mackenbach](https://yarmo.eu), a big fan of delightful lists and FOSS in general.

This website itself is [open source](https://codeberg.org/yarmo/delightful-club) (hosted on [Codeberg.org](https://codeberg.org)) and dedicated to the public domain ([CC0-1.0 license](https://creativecommons.org/publicdomain/zero/1.0/)).

CSS framework: [Simple.css](https://simplecss.org/)  
Tracking: none