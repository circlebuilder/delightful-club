use std::fs;

extern crate config;
extern crate serde;

#[macro_use]
extern crate serde_derive;

mod settings;

use settings::Settings;
use settings::List;

async fn update_page(title: &str, source: &str, repository: &str) -> Result<(), Box<dyn std::error::Error>> {
    let tag = &title.replace(" ", "_").to_lowercase();

    println!("{}: downloading README.md...", &title);

    let header = format!("+++
    title = \"{}\"
    slug = \"{}\"
    
    [extra]
    repository = \"{}\"
+++
", &title, &tag, &repository);

    let resp = reqwest::get(source)
        .await?
        .text()
        .await?;

    let content = [header, resp].join("\n");

    println!("{}: saving to file...", &title);

    fs::write(format!("website/content/{}.md", &tag), content)?;

    println!("{}: done!", &title);
    Ok(())
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let settings = Settings::new();

    for list in &settings.unwrap().lists {
        let list: &List = list;
        update_page(&list.title, &list.source, &list.repository).await?;
    }

    Ok(())
}