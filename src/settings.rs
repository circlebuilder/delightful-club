use config::{ConfigError, Config, File};

#[derive(Debug, Deserialize)]
pub struct List {
    pub title: String,
    pub source: String,
    pub repository: String,
}

#[derive(Debug, Deserialize)]
pub struct Settings {
    pub lists: Vec<List>
}

impl Settings {
    pub fn new() -> Result<Self, ConfigError> {
        let mut s = Config::default();
        s.merge(File::with_name("config.toml"))?;
        s.try_into()
    }
}
